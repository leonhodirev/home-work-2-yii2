<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $project common\models\Project */
/* @var $role */

?>
<div class="password-reset">
    <p>Привет <?= $user->username ?>,</p>

    <p>В проекте <?= $project->title ?>, у тебя роль <?= $role?></p>
</div>
