<?php

namespace common\modules\chat\widgets;

use common\modules\chat\widgets\assets\ChatAsset;
use yii\bootstrap\Widget;

class Chat extends Widget
{
    public function init()
    {
        ChatAsset::register($this->view);
    }

    public function run()
    {
        return $this->render('chat');
    }
}