<?php

namespace common\services;

use yii\base\Component;
use common\models\Project;
use common\models\ProjectUser;
use common\models\User;
use common\models\Task;
use Yii;

class TaskService extends Component
{

    public function canManager(Project $project, User $user)
    {
        return Yii::$app->projectService->hasRole($project, $user, ProjectUser::ROLE_MANAGER);
    }

    public function canTake(Project $project, User $user)
    {
        return Yii::$app->projectService->hasRole($project, $user, ProjectUser::ROLE_DEVELOPER);
    }

    public function canComplete(Task $task, User $user)
    {
        if ($task->executor_id == $user->id && empty($task->completed_at)) {
            return true;
        } else {
            return false;
        }
    }

    public function takeTask(Task $task, User $user)
    {
        $task->executor_id = $user->id;
        $task->started_at = time();
        return $task->save();
    }

    public function completeTask(Task $task, User $user)
    {
        $task->completed_at = $user->id;
        return $task->save();
    }
}