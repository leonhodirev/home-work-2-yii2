<?php

namespace common\services;

use Yii;
use yii\base\Component;

class EmailService extends Component
{
    public function send($email, $subject, $views, $data)
    {
        Yii::$app->mailer
            ->compose($views, $data)
            ->setTo($email)
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setSubject($subject)
            ->send();
    }
}