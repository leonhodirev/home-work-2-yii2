<?php

namespace frontend\modules\api\controllers;

use frontend\modules\api\models\Project;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBasicAuth;
use yii\rest\Controller;

class ProjectController extends Controller
{
    public $modelClass = 'frontend\modules\api\models\Project';

    public function actionIndex()
    {
        return new ActiveDataProvider([
            'query' => Project::find(),
        ]);
    }

    public function actionView($id)
    {
        return Project::findOne($id);
    }

//    public function behaviors()
//    {
//        $behaviors = parent::behaviors();
////        $behaviors['authenticator'] = [
////            'class' => HttpBasicAuth::className(),
////        ];
//        return $behaviors;
//    }
//
//    public function actions()
//    {
//        $actions = parent::actions();
//// отключить действия "delete" и "create"
//        unset($actions['delete'], $actions['create']);
//// настроить подготовку провайдера данных с помощью метода prepareDataProvider()
//        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
//        return $actions;
//    }
//
//    public function prepareDataProvider()
//    {
//        return new ActiveDataProvider([
//            'query' => Project::find()
//        ]);
//    }
//
//    public function checkAccess($action, $model = null, $params = [])
//    {
//        if ($action === 'update' || $action === 'delete') {
//            if ($model->id_user !== \Yii::$app->user->id)
//                throw new \yii\web\ForbiddenHttpException(sprintf('Вы можете
//выполнять %s только с теми проектами, который созданы Вами.', $action));
//        }
//    }
}