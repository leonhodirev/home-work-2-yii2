<?php

namespace frontend\modules\api\models;


class User extends \common\models\User
{
    public function fields()
    {
        return ['id'];
    }

    public function extraFields()
    {
        return [
            'projectsUsers', 'projects'
        ];
    }
}