<?php

namespace frontend\modules\api\models;


class Project extends \common\models\Project
{
    public function fields()
    {
        return [
            'id',
            'title' => function (Project $model){
                return $model->title . ' ' . User::STATUSES[$model->active];
            }
        ];
    }

    public function extraFields()
    {
        return [
            'projectsUsers'
        ];
    }
}