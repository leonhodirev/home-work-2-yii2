var conn = new WebSocket('ws://localhost:8080');

conn.onopen = function (e) {
    console.log("Connection established!");
};

conn.onmessage = function (e) {
    console.log(e.data);
    document.getElementById('chatMessages').value = e.data + '\n' + document.getElementById('chatMessages').value;
};

conn.onerror = function (e) {
    console.log("Connection fail!");
};