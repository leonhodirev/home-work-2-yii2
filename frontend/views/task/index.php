<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\TaskSearch */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <p>
        <?= Html::a('Create Task', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'title',
            [
                'attribute' => 'project_id',
                'filter' => \common\models\Project::find()->byUser(Yii::$app->user->id)->select('title')->indexBy('id')->column(),
                'value' => function (\common\models\Task $model) {
                    return Html::a($model->project->title, ['project/view', 'id' => $model->project_id], ['target' => '_blank']);
                },
                'format' => 'html'
            ],
            'description:ntext',
            'estimation',
            'executor_id',
            [
                'attribute' => 'created_by',
                'filter' => \common\models\User::find()->onlyActive()->select('username')->indexBy('id')->column(),
                'value' => function (\common\models\Task $model) {
                    return Html::a($model->createdBy->username, ['user/view', 'id' => $model->createdBy->id], ['target' => '_blank']);
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'updated_by',
                'filter' => \common\models\User::find()->onlyActive()->select('username')->indexBy('id')->column(),
                'value' => function (\common\models\Task $model) {
                    return Html::a($model->updatedBy->username, ['user/view', 'id' => $model->updatedBy->id], ['target' => '_blank']);
                },
                'format' => 'html'
            ],
            //'started_at',
            //'completed_at',
            //'created_by',
            //'updated_by',
            //'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {take} {complete}',
                'visibleButtons' => [
                    'update' => function ($model) {
                        return Yii::$app->taskService->canManager($model->project, Yii::$app->user->identity);
                    },
                    'delete' => function ($model) {
                        return Yii::$app->taskService->canManager($model->project, Yii::$app->user->identity);
                    },
                    'take' => function ($model) {
                        return Yii::$app->taskService->canTake($model->project, Yii::$app->user->identity);
                    },
                    'complete' => function ($model) {
                        return Yii::$app->taskService->canTake($model->project, Yii::$app->user->identity) &&
                            Yii::$app->taskService->canComplete($model, Yii::$app->user->identity);
                    }
                ],
                'buttons' => [
                    'take' => function ($url, \common\models\Task $model, $key) {
                        return Html::a(Html::icon('hand-right'), ['task/take', 'id' => $model->id], ['data' => [
                            'confirm' => 'Берёшь задачу?',
                            'method' => 'post'
                        ]]);
                    },
                    'complete' => function ($url, \common\models\Task $model) {
                        return Html::a(Html::icon('hand-right'), ['task/complete', 'id' => $model->id], ['data' => [
                            'confirm' => 'Завершаем?',
                            'method' => 'post'
                        ]]);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
