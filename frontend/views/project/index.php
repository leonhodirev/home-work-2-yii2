<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\ProjectSearch */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <!--    <p>-->
    <!--        --><? //= Html::a('Create Project', ['create'], ['class' => 'btn btn-success']) ?>
    <!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'title',
            [
                'attribute' => 'title',
                'value' => function (\common\models\Project $model) {
                    return Html::a($model->title, ['view', 'id' => $model->id]);
                },
                'format' => 'html'
            ],
            [
                'attribute' => \common\models\Project::RELATION_PROJECT_USERS . '.role',
                'value' => function (\common\models\Project $model) {
                    return join(',', Yii::$app->projectService->getRoles($model, Yii::$app->user->identity));
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'active',
                'filter' => \common\models\Project::STATUSES,
                'value' => function (\common\models\Project $model) {
                    return \common\models\Project::STATUSES[$model->active];
                }
            ],
            'description:ntext',
            [
                'attribute' => 'created_by',
                'value' => function (\common\models\Project $model) {
                    return Html::a($model->createdBy->username, ['user/view', 'id' => $model->createdBy->id], ['target' => '_blank']);
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'updated_by',
                'value' => function (\common\models\Project $model) {
                    return Html::a($model->updatedBy->username, ['user/view', 'id' => $model->updatedBy->id], ['target' => '_blank']);
                },
                'format' => 'html'
            ],
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => ''
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
