<?php

namespace frontend\tests\functional;

use frontend\tests\FunctionalTester;

class FirstCest
{
    /**
     * @var \frontend\tests\FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests

    /**
     * @dataProvider pageProvider
     */
    public function testSomeFeature(FunctionalTester $I, \Codeception\Example $example)
    {
        $I->amOnPage($example['url']);
//        $I->see($example['title'], 'h1');
        $I->seeInTitle($example['title']);
    }

    /**
     * @return array
     */
    protected function pageProvider() // alternatively, if you want the function to be public, be sure to prefix it with `_`
    {
        return [
            ['url' => "/", 'title' => "My Yii Application"],
//            ['url' => "site/hello", 'title' => "Hello, world"],
            ['url' => "site/login", 'title' => "Login"],
            ['url' => "site/contact", 'title' => "Contact"]
        ];
    }
}