<?php
namespace frontend\tests;

use frontend\models\ContactForm;

class FirstTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testSomeFeature()
    {
        $a = 5;
        $this->assertTrue($a == 5);
        $this->assertEquals(5, $a);
        $this->assertLessThan(6, $a);

        $model = new ContactForm();

        $model->attributes = [
            'name' => 'Tester',
            'email' => 'tester@example.com',
            'subject' => 'very important letter subject',
            'body' => 'body of current message',
        ];

        $this->assertAttributeEquals('Tester', 'name', $model);
        $this->assertAttributeEquals('tester@example.com', 'email', $model);
        $this->assertAttributeEquals('very important letter subject', 'subject', $model);
        $this->assertAttributeEquals('body of current message', 'body', $model);

        $this->assertArrayHasKey('name', $model);
        $this->assertArrayHasKey('email', $model);
        $this->assertArrayHasKey('subject', $model);
        $this->assertArrayHasKey('body', $model);
    }
}