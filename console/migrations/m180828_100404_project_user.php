<?php

use yii\db\Migration;

class m180828_100404_project_user extends Migration
{
    public function up()
    {
        $this->createTable('{{%project_user}}', [
            'project_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'role' => 'ENUM("manager", "developer", "admin") NOT NULL', //ALTER TABLE project ADD role ENUM('yes', 'no') NOT NULL;
        ]);

        $this->addForeignKey('fx_project_user_project', 'project_user', 'project_id',
            'project', 'id');

        $this->addForeignKey('fx_project_user_user', 'project_user', 'user_id',
            'user', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fx_project_user_project', 'project_user');
//
        $this->dropForeignKey('fx_project_user_user', 'project_user');

        $this->dropTable('{{%project_user}}');
    }
}
