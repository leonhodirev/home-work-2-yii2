<?php

use yii\db\Migration;

class m180828_100343_project extends Migration
{
    public function up()
    {
        $this->createTable('{{%project}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('fx_project_user_created_by', 'project', 'created_by',
            'user', 'id');

        $this->addForeignKey('fx_project_user_updated_by', 'project', 'updated_by',
            'user', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fx_project_user_created_by', 'project');

        $this->dropForeignKey('fx_project_user_updated_by', 'project');

        $this->dropTable('{{%project}}');
    }
}
