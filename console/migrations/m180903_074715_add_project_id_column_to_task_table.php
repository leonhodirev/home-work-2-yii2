<?php

use yii\db\Migration;

/**
 * Handles adding project_id to table `task`.
 */
class m180903_074715_add_project_id_column_to_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task', 'project_id', $this->integer()->notNull());

        $this->addForeignKey('fx_task_project_project_id', 'task', 'project_id',
            'project', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('task', 'project_id');

        $this->dropForeignKey('fx_task_project_project_id', 'task');
    }
}
