<?php

use yii\db\Migration;

class m180828_100333_task extends Migration
{
    public function up()
    {
        $this->createTable('{{%task}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'estimation' => $this->integer()->notNull(),
            'executor_id' => $this->integer(),
            'started_at' => $this->integer(),
            'completed_at' => $this->integer(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('fx_task_user_executor_id', 'task', 'executor_id',
            'user', 'id');

        $this->addForeignKey('fx_task_user_created_by', 'task', 'created_by',
            'user', 'id');

        $this->addForeignKey('fx_task_user_updated_by', 'task', 'updated_by',
            'user', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fx_task_user_executor_id', 'task');

        $this->dropForeignKey('fx_task_user_created_by', 'task');

        $this->dropForeignKey('fx_task_user_updated_by', 'task');

        $this->dropTable('{{%task}}');
    }
}
