<?php

namespace console\controllers;

use yii\console\Controller;

class ConsoleController extends Controller
{
    public function actionGreatings()
    {
        echo 'Hello, world';
    }
}